package com.zuitt;

public class Course {

    private String name;
    private String description;
    private int seats;
    private double fee;
    private String startDate;
    private String endDate;
    private String instructor;


    //Empty Constructor
        public Course(){};


    //[GETTER] for Course
        public String getCourseName() { return this.name; }
        public String getCourseDesc() { return this.description; }
        public int getSeats() { return this.seats; }
        public double getFee() { return this.fee; }
        public String getStartDate() { return this.startDate; }
        public String getEndDate() { return this.endDate; }
        public String getInstructor() { return this.instructor; }


    //[SETTER] for Course
        //We use 'PUBLIC VOID'
        public void setCourseName(String name) { this.name = name; }
        public void setCourseDesc(String description) { this.description = description; }
        public void setSeats(int seats) { this.seats = seats; }
        public void setFee(double fee) { this.fee = fee; }
        public void setStartDate(String startDate) { this.startDate = startDate; }
        public void setEndDate(String endDate) { this.endDate = endDate; }
        public void setInstructor(String instructor) { this.instructor = instructor; }


}
