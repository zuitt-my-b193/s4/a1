package com.zuitt;

public class User {

    private String firstName;
    private String lastName;
    private int age;
    private String address;

    public User(String firstName, String lastName, int age, String address){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
    }

    //[GETTER] for User
        public String getFirstName() { return this.firstName; }
        public String getLastName() { return this.lastName; }
        public int getAge() { return this.age; }
        public String getAddress() {return this.address;}
}
