package com.zuitt;

public class Main {

    public static void main(String[] args){

        //[INSTANTIATION] of Object
            //New instance of User Object with parameterized parameter
            User newUser = new User("Tee Jae", "Calinao", 25, "Antipolo City");

        //[GETTERS] for User
            System.out.println("User's first name: " + newUser.getFirstName());

            System.out.println("User's last name: " + newUser.getLastName());

            System.out.println("User's age: " + newUser.getAge());

            System.out.println("User's address: " + newUser.getAddress());


        //[SETTERS] for Course
            Course newCourse = new Course();
            newCourse.setCourseName("Physics 101");
            newCourse.setCourseDesc("Learn Physics");
            newCourse.setSeats(30);
            newCourse.setFee(1000.58);
            newCourse.setStartDate("July 4, 2022");
            newCourse.setEndDate("July 20, 2022");
            newCourse.setInstructor("Tee Jae Calinao");

            System.out.println("Course Name: " + newCourse.getCourseName());
            System.out.println("Course Description: " + newCourse.getCourseDesc());
            System.out.println("Course Seats: " + newCourse.getSeats());
            System.out.println("Course Fee: " + newCourse.getFee());
            System.out.println("Course Start Date: " + newCourse.getStartDate());
            System.out.println("Course End Date: " + newCourse.getEndDate());
            System.out.println("Course Instructor's Name: " + newCourse.getInstructor());


    }
}
